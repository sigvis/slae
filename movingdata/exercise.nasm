; Hello World example
; Author: Douglas
; Moving Datatypes exercise

global _start

section .text
_start:
	; Make initial message
	mov eax, 0x4
	mov ebx, 0x1
	mov ecx, message
	mov edx, mlen
	int 0x80

	; Push and pop from the stack
	push pmessage
	pop ecx
	mov edx, pmlen
	mov eax, 0x4 ; this has to be reset since the syscall above will return the length of the string written.
	int 0x80
	
	; Exit the program
	mov eax, 0x1
	mov ebx, 0xA
	int 0x80
	

section .data
	message: db "Push and pop from the stack"
	mlen equ $-message
	pmessage: db "some stack data"
	pmlen equ $-pmessage

