; testing... 1, 2, 3 .... testing

global _start

section .text
printing:
	mov al, 4
	mov bl, 1
	push edx
	xor edx, edx
	mov edx, [esp]
	int 0x80
	add esp,4
	ret

exit:
	xor eax, eax
	xor ebx, ebx
	mov al, 1
	mov bl, 1
	int 0x80
	

_start:
	xor eax,eax
	xor ebx,ebx
	xor ecx,ecx
	xor edx,edx
	mov ecx, sIP
	mov edx, iplen
	call printing
	call exit



section .data
	sIP: db "127.0.0.1"
	iplen: equ $-sIP
