; Filename: exit.nasm
; Author: Will
; Purpose: show how to exit shellcode?

global _start

section .text
_start:
	
	xor eax, eax ; We are trying to fill eax with 0s so we will xor with itself to fill it up.
	mov al, 0x1
	xor ebx, ebx
	mov bl, 0xA
	int 0x80
