; Author: Will
; Purpose: Create the hello world app with libc calls instead of syscalls

extern printf
extern exit

global main

section .text
main:
	push message
	call printf
	add esp, 0x4 ; adjust the stack. Somehow ensure that the stack is back to where it was before.
	mov eax, 0xA
	call exit

section .data
	message: db "Hello World!", 0xA, 0x00
	mlen equ $-message


