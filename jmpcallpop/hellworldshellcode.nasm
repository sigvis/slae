; Hello World Shellcode example
; Author: Douglas

global _start

section .text
_start:
	jmp short call_shellcode

shellcode:
	xor eax, eax
	mov al, 0x4
;	mov eax, 0x4
	xor ebx, ebx
	mov bl, 0x1
;	mov ebx, 0x1
	pop ecx ; used to get the address of message. How does this actually get the address of message when we didn't move the message variable into ecx? Because the call method puts the following address onto the stack in order to know where to return to.
;	mov ecx, message
	xor edx, edx
	mov dl, 13
;	mov edx, mlen
	int 0x80
	
	; Exit the program
	xor eax, eax
	mov al, 0x1
	;mov eax, 0x1
	xor ebx, ebx
	;mov ebx, 0xA
	int 0x80
	
call_shellcode:
	call shellcode
	message: db "Hello World!", 0xA

section .data
