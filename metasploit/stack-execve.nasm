; Author Will
; Stack execve

global _start
section .text
_start: 
	xor eax, eax
	push eax

	; ./reverse.py "////bin/bash"
	; String length : 12
	; hsab : 68736162
	; /nib : 2f6e6962
	; //// : 2f2f2f2f

	push 0x68736162
	push 0x2f6e6962
	push 0x2f2f2f2f

	mov ebx, esp

	push eax
	mov edx, esp

	push ebx
	mov ecx, esp

	mov al, 11
	int 0x80
