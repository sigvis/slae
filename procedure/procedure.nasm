; Author Will
; Purpose: Procedures

global _start

section .text

HelloWorldProc:
	; print hello world with syscall

	mov eax, 0x4
	mov ebx, 0x1
	mov ecx, message
	mov edx, mlen
	int 0x80
	ret ; signifies end of procedure

_start:
	mov ecx, 0x10

PrintHelloWorld:
	push ecx
	call HelloWorldProc
	pop ecx
	loop PrintHelloWorld

	mov eax, 1
	mov ebx, 0xA
	int 0x80
	ret

section .data

	message: db "Hello World!"
	mlen equ $-message
